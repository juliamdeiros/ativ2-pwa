﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Buffet.Models;
using Microsoft.AspNetCore.Authorization;

namespace Buffet.Controllers.Admin
{
    [Authorize]
    public class AdminController : Controller
    {
        public AdminController()
        {
        }

        public IActionResult Supervisao()
        {
            return View();
        }
    }
}
