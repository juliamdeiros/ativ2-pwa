﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Buffet.Models;
using Buffet.Models.Acesso;

namespace Buffet.Controllers
{
    public class AcessoController : Controller
    {
        private readonly AcessoService _acessoService;

        public AcessoController(AcessoService acessoService)
        {
            _acessoService = acessoService;
        }

        public IActionResult Login()
        {
            //_clienteService.ObterClientes();

            return View();
        }

        public IActionResult RecuperarConta()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Cadastrar()
        {
            var viewmodel = new CadastroViewModel();
            viewmodel.Mensagem = (string)TempData["msg-cadastro"];
            viewmodel.ErrosCadastro = (string[])TempData["erros-cadastro"];
            return View(viewmodel);
        }

        [HttpPost]
        public async Task<RedirectToActionResult> Cadastrar(AcessoCadastrarRequestModel request)
        {
            var email = request.Email;
            var senha = request.Senha;
            var senhaConfirmacao = request.SenhaConfirmacao;

            if (email == null)
            {
                TempData["msg-cadastro"] = "Por favor, insira um email";
                return RedirectToAction("Cadastrar");
            }

            try
            {
                await _acessoService.RegistrarUsuario(email, senha);
                TempData["msg-cadastro"] = "Cadastro realizado com sucesso, faça o login!";
                return RedirectToAction("Login");

            }
            catch (CadastrarUsuarioException e)
            {
                var listaErros = new List<string>();

                foreach (var IdentityError in resultado.Errors)
                {
                    listaErros.Add(IdentityError.Description);
                }
                TempData["erros-cadastro"] = listaErros;
                return RedirectToAction("Cadastrar");
            }
            return Redirect(redirectURL);
        }
    }
}
