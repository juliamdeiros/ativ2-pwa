﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Buffet.Models;

namespace Buffet.Controllers
{
    public class InformacoesLegaisController : Controller
    {
        public InformacoesLegaisController()
        {
        }

        public IActionResult TermosDeUso()
        {
            return View();
        }

        public IActionResult PoliticaDePrivacidade()
        {
            return View();
        }
    }
}


