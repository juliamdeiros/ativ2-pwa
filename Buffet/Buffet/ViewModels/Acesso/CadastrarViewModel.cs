﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.ViewModels.Acesso
{
    public class CadastrarViewModel
    {
        public String Mensagem { get; set; }
        public String[] ErrosCadastro { get; set; }
    }
}

