﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.RedirectModels
{
    public class AcessoCadastrarRequestModel
    {
        public String Email { get; set; }
        public String Senha { get; set; }
        public String SenhaConfirmacao { get; set; }
    }
}
