﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.Models.Acesso
{
    public class AcessoService
    {
        private readonly Microsoft.AspNetCore.Identity.UserManager<Usuario> _userManager;
        private readonly Microsoft.AspNetCore.Identity.SignInManager<Usuario> _signInManager;

        public AcessoService(UserManager<Usuario> userManager, SignInManager<Usuario> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public async Task AutenticaUsuario(string username, string senha)
        {
            var resultado = await _signInManager.PasswordSignInAsync(username, senha, false, false);
            if (!resultado.Succeeded)
            {
                throw new Exception("Usuário ou senha invalidos");
            }
        }

        public async Task<IAsyncResult> RegistraUsuarioAsync(string email, string senha)
        {
            var novoUsuario = new Usuario() { UserName = email, Email = email };

            var resultado = await _userManager.CreateAsync(novoUsuario, senha);


            if (!resultado.Succeeded)
            {
                throw new CadastrarUsuarioException(resultado.Errors);
            }
        }
    }
}
