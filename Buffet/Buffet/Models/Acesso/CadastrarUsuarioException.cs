﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Buffet.Models.Acesso
{
    public class CadastrarUsuarioException
    {
        public IEnumerable<IdentityError> Error { get; set; }

        public CadastrarUsuarioException(IEnumerable<IdentityError> error)
        {
            Error = error;
        }
    }
}

