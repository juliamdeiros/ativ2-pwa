﻿using System;
using System.Collections.Generic;
using System.Linq;
using Buffet.Data;
using Microsoft.EntityFrameworkCore;

namespace Buffet.Models.Buffet.Cliente
{
    public class ClienteService
    {
        private readonly DatabaseContext _databaseContext;

        public ClienteService(
            DatabaseContext databaseContext
        )
        {
            _databaseContext = databaseContext;
        }

        public void ObterClientes()
        {
            // ENTIDADES RELACIONADAS
            var cliente = _databaseContext.Clientes
                .Include(c => c.Eventos)
                .ToList()
                .Single(c => c.Id.ToString()
                    .Equals("08d8f887-cd09-421d-8ee1-a0f9f0d36e57")
                );

            if (cliente != null)
            {
                Console.WriteLine(cliente.Id);
                Console.Write(" :: " + cliente.Nome);
                Console.Write(" :: " + cliente.Email);
                Console.Write(" :: " + cliente.Eventos.Count);
            }
        }
    }
}
